require('dotenv').config();
const express = require('express');
const needle = require('needle');
const cors = require('cors')


const app = express();
const port = 3000;
const token = process.env.TWITTER_BEARER_TOKEN;
const endpointUrl = 'https://api.twitter.com/2/tweets/search/recent';
const twitterAccount = process.env.TWITTER_FOLLOWED_USER;

/**
 * Twitter client (gets last tweets + pagination)
 * @see https://github.com/twitterdev/Twitter-API-v2-sample-code/blob/master/Recent-Search/recent_search.js
 */
const getTweets = async (nextToken, untilId) => {

  // Edit query parameters below
  const params = {
    'query': `from:${twitterAccount}`,
    'max_results': 20
  };

  if (nextToken) {
    params.next_token = nextToken;
  }

  if (untilId) {
    params.until_id = untilId;
  }

  const res = await needle('get', endpointUrl, params, { headers: {
      "authorization": `Bearer ${token}`
  }});

  if(res.body) {
      return res.body;
  } else {
      throw new Error ('Unsuccessful request');
  }
}

app.use(cors());

// app.get('/', (req, res) => {
//   res.send('Hello World!');
// });

app.get('/tweets', async (req, res, next) => {
  const nextToken = req.query.next || null;
  const oldestId = req.query.oldest_id || null;

  try {
    const tweets = await getTweets(nextToken, oldestId);
    res.json(tweets);
  } catch (e) {
    //this will eventually be handled by your error handling middleware
    next(e);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
